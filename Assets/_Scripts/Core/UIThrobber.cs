﻿using UnityEngine;
using System.Collections;

public class UIThrobber : MonoBehaviour 
{
	private RectTransform _RectTransform;

	void Start () 
	{
		this._RectTransform = this.GetComponent <RectTransform> ();
	}

	public float speed = 0.1f;

	void Update () 
	{
		this._RectTransform.localScale = new Vector3 (1.0f + Hermite (Mathf.PingPong (Time.time * this.speed, 0.15f)), 
		                                              1.0f + Hermite (Mathf.PingPong (Time.time * this.speed, 0.15f)), 
		                                              0);
	}

	private float Hermite (float t)
	{
		return -t * t * t * 2f + t * t * 3f;
	}
}
