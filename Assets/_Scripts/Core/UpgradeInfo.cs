﻿using UnityEngine;
using System.Collections;

public static class UpgradeInfo 
{
	// --
	// -- Magnet
	// --

	private static float[] Magnet_Powers = new float[]
	{
		0.6f, // level 0 // 0.5
		1.0f, // level 1 // 1.0
		1.3f, // level 2 // 1.3
		1.6f,  // level 3 // 1.6
        2.3f,   //             2.0
        2.4f,
        2.41f,
        2.42f,
        2.43f,
        2.44f,
        2.5f
	};

	private static int[] MagnetCosts = new int[]
	{
		1300,  // level 1
		5000,  // level 2
		13000, // level 3
        25000,
        130000,
        130000,
        130000,
        130000,
        130000,
		1300000,
        1300000

	};

	public static float GetMagnetPower (int level)
	{
		return UpgradeInfo.Magnet_Powers [level];
	}

	public static int GetMagnetCost (int level)
	{
		return UpgradeInfo.MagnetCosts [level];
	}

	// -- 
	// -- Multiplier 
	// -- 

	private static int[] Multiplier_Powers = new int[]
	{
		1, // level 0 // default ... 
		3, // level 1 // 
		6, // level 2 // 
		15,  // level 3 // 
        20,
        25,
        28,
        30,
        32,
        40,
        40


	};
	
	private static int[] MultiplierCosts = new int[]
	{
		1300,  // level 1
		5000,  // level 2
		13000, // level 3
        25000,
        130000,
        130000,
        130000,
        130000,
        130000,
        1300000
	};
	
	public static float GetMultiplierPower (int level)
	{
            return UpgradeInfo.Multiplier_Powers[level];
        
	}
	
	public static int GetMultiplierCost (int level)
	{
		return UpgradeInfo.MultiplierCosts [level];
	}

	// -- 
	// -- Speed 
	// -- 

	private static int[] JumpSpeedCosts = new int[]
	{
		2000,  // level 1
		6000,  // level 2
		10000, // level 3
        30000,
        100000,
        100000
	};

	public static int GetJumpSpeedCost (int level)
	{
		return UpgradeInfo.JumpSpeedCosts [level];
	}
}
