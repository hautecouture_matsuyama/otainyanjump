﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIChanger : MonoBehaviour {

    private const int iphoneX_height = 1125;
    private const int iphoneX_width = 2436;

    private void Awake()
    {
        Vector2 sd = GetComponent<RectTransform>().sizeDelta;
        sd.x = 980;

        if (Screen.width == iphoneX_width && Screen.height == iphoneX_height)
        {
            this.gameObject.GetComponent<RectTransform>().sizeDelta = sd;
            Debug.Log("iphoneX");
        }

    }
}
