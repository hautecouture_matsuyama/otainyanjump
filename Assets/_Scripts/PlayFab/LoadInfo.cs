﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadInfo : MonoBehaviour
{
    public static void LoadAllInfo()
    {
        GameManager.instance.name_server = PlayerPrefs.GetString("PLAYERNAME");
        GameManager.instance.login_int = PlayerPrefs.GetInt("LOGININT");
        GameManager.instance.userid_server = PlayerPrefs.GetString("USERID");

    }
}
