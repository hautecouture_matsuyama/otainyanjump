﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SocialConnector;
using UnityEngine.UI;

public class ShareController : MonoBehaviour
{
    public void Share()
    {
        StartCoroutine(_Share());
    }

    public IEnumerator _Share()
    {
        string imgPath = Application.persistentDataPath + "/image.png";

        //前回のデータ消去
        File.Delete(imgPath);

        //スクリーンショットを投稿
        ScreenCapture.CaptureScreenshot("image.png");


        //投稿映像の保存が完了するまで待機

        while (true)
        {
            if (File.Exists(imgPath)) break;
            yield return null;
        }

        //投稿する
        string tweetText = "I have just scored " + GameManager.instance.currentScore + " in The Jump! Can you beat me ? ";
        string tweetURL = "";
        SocialConnector.SocialConnector.Share(tweetText, tweetURL, imgPath);


    }
}
