﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingText : MonoBehaviour {

    [SerializeField]
    private Text loadText;

    void OnEnable()
    {
        StartCoroutine(Loading());

    }


    IEnumerator Loading()
    {
        while(this.gameObject.activeSelf)
        {
            loadText.text = "読込中";
            yield return new WaitForSeconds(0.5f);
            loadText.text = "読込中.";
            yield return new WaitForSeconds(0.5f);
            loadText.text = "読込中..";
            yield return new WaitForSeconds(0.5f);
            loadText.text = "読込中...";
            yield return new WaitForSeconds(0.5f);

        }
    }


}
