﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseButton : MonoBehaviour {

    [SerializeField]
    private GameObject closeObject;

    public void Close()
    {
        closeObject.SetActive(false);
        Game.instance.ResumeGame();
    }

}
